/**
 * @file
 */

(function ($) {

  Drupal.behaviors.ckeditor_socialmedia = {

     attach: function (context, settings) {

      // Live stream.
      if ($('div.cklivestream')) {
        $('div.cklivestream').hide().each(function () {
          lswidth = 500;
          lsheight = 307;
          lsfitwidth = $(this).parent().width();
          lsfitheight = Math.floor(lsfitwidth / lswidth * lsheight);
          lsurl = encodeURIComponent($(this).data('username'));
          // note: using HTTP for <a href> because Livestream has certificate problems.
          $(this).before('<iframe width="' + lsfitwidth + '" height="' + lsfitheight + '" src="//cdn.livestream.com/embed/' + lsurl + '?layout=4&width=' + lsfitwidth + '&height=' + lsfitheight + '&color=0x' + Drupal.settings.ckeditor_socialmedia.livestream_color + '&iconColor=0x' + Drupal.settings.ckeditor_socialmedia.livestream_icon_color + '&iconColorOver=0x' + Drupal.settings.ckeditor_socialmedia.livestream_icon_color_over + '" style="border:0;outline:0" frameborder="0" scrolling="no"></iframe><div class="uw_video-embed-link"><a href="http://www.livestream.com/' + lsurl + '">Watch video on Livestream</a></div>');
        });
      }
      // Facebook.
      if ($('div.ckfacebook')) {
          $('div.ckfacebook').each(function () {
              fbwidth = $(this).parent().width();
              // Set sidebar width as a facebook widget width in order to display facebook in promo-items.
              if (fbwidth == 0 && $(this).parents('#site-sidebar').length) {
                // RT#926529
                // This is the default width for the sidebar, setting it
                // to ensure that an JS run after does not have a width of 0.
                fbwidth = 239;
              }
              if ($(this).data('type') == 'singlepost') {
                  fburl = $(this).data('fburl');
                  fullpost = $(this).data('fullpost');
                  $(this).before('<div class="fb-post" data-href="' + fburl + '" data-width="' + fbwidth + '" data-show-text="' + fullpost + '"></div>');
                  $(this).remove();

                  // Facebook javascript SDK.
                  $('div.ckfacebook a').hide().attr('width', $('div.ckfacebook').parent().width());
                  !function (d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) {
                          return;
                      }
                      js = d.createElement(s); js.id = id;
                      js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.1';
                      fjs.parentNode.insertBefore(js, fjs);
                  }(document, 'script', 'facebook-jssdk');
              }
              else {
                  // Anything that isn't a single post is assumed to be a timeline.
                  // This allows widgets created before we supported anything else to work.
                  fburl = encodeURIComponent('/' + $(this).data('username'));
                  $(this).before('<iframe src="https://www.facebook.com/plugins/likebox.php?href=' + fburl + '&amp;width=' + fbwidth + '&amp;height=350&amp;colorscheme=' + Drupal.settings.ckeditor_socialmedia.facebook_colorscheme + '&amp;show_faces=false&amp;border_color&amp;stream=true&amp;header=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; max-width: 500px; width:' + fbwidth + 'px; height:350px;" allowTransparency="true"></iframe>');
                  $(this).remove();
              }
          });
      }

      // New Twitter.
      $('blockquote.twitter-tweet').hide();
      $('div.cktwitter a').hide().attr('width', $('div.cktwitter').parent().width());
      !function (d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (!d.getElementById(id)) {
              js = d.createElement(s);
              js.id = id;
              js.src = "//platform.twitter.com/widgets.js";
              fjs.parentNode.insertBefore(js, fjs);
          }
          // Code from Twitter to activate widgets.
      }(document, "script", "twitter-wjs");
      // If a new Twitter widget failed to load after 2 seconds, show the link (if Twitter does eventually load, it will hide the link itself).
      setTimeout(function () {
          // Standard Twitter widgets.
          $.each($('div.cktwitter'), function () {
              $iframe = $('iframe', this);
              if (!$iframe.length || $iframe.contents().find('body').html() == '') {
                  $(this).closest('div').find('a').show();
              }
          });
          // Embedded Tweets (Twitter removes the blockquote if it succeeds).
          $('blockquote.twitter-tweet').show();
          }, 2000);
     }
  };
})(jQuery);
