This module provides social media (currently Facebook, Twitter and Livestream)
widget support within CKEditor in the Wysiwyg module, using the platform's
provided widgets, without having to allow <script> tags.

Once you enable this module, you should add the Facebook, Twitter and Livestream
buttons from the WYSIWYG profiles for CKEditor for your text formats, and enable
the CKEditor link filter within your text formats. If you are filtering allowed
HTML tags and/or attributes, you must allow the following tags and attributes to
bypass the filter.

* cktwitter[data-type|data-username|data-listname|data-displayname]
* ckfacebook[data-username|data-displayname]
* cklivestream[data-username|data-displayname]

This module will replace the custom tags with JavaScript widgets from the social
media site. If JavaScript is disabled, the widgets will fall back to a link to
the content. Note that Twitter sets a cookie when visited with JavaScript on; if
you have visited the site with JavaScript on and then come back with JavaScript
off, the links will not work as expected as Twitter is expecting you to have
JavaScript on based on its cookie.

Widget colour schemes can be customized site-wide, to the extent the platform's
widget allows, using the module's settings page. At this time, it is not
possible to adjust the colour scheme for an individual widget.

Current supported widgets:

* Twitter profile widget * Twitter list widget * Twitter likes widget * Facebook
"like box" (for Facebook Pages) * Livestream live streaming video widget


Facebook, Twitter, and LiveStream widget dialog box options

FACEBOOK

1. Facebook page name: facebook.com/ This is your page URL, minus the domain
name and protocol (e.g. http://facebook.com/). The widget supports pages with
usernames (e.g. university.waterloo) and pages following the
pages/pagename/number format (e.g. pages/Ignite-Waterloo/133592706395).

2. Display name: This property is ignored by the Facebook widget (it uses the
actual name of your page, regardless of what you enter here), but is used to
display the name of the Facebook page in the hover text of the placeholder in
CKEditor. It is also used on the live page if JavaScript is disabled - in place
of the widget, a text link is displayed, showing "Facebook: " and then your
display name.


TWITTER

1. Twitter widget type: This select list allows you to choose between widgets
for Twitter profiles, likes, and lists. The Twitter profile widget shows the
user's most recent tweets. The faves widget shows the user's most recent
favourited tweets (if the user does not have any favourited tweets, the widget
will be empty). The list widget shows the most recent tweets from a user's
public Twitter list (lists that have been made private are not supported).

2. Twitter user name: @ This is shown for all widget types, and is the desired
feed's Twitter username, without the "@" symbol.

3. List name: This is only shown for list widgets, and is the Twitter list name.
This list must be publicly available in the user's Twitter account.

4: Display name: This is shown for all widget types. It is used to display the
name of the Twitter feed in the hover text of the placeholder in CKEditor. It is
also used on the live page if JavaScript is disabled - in place of the widget, a
text link is displayed, showing "Twitter: "/"Twitter favourites: "/"Twitter
list: " and then your display name. With JavaScript enabled, the display name
appears at the top of the Twitter widget, with the exception of the profile
widget, which will use the actual profile name and username.


LIVESTREAM

1. Livestream channel page name: livestream.com/ This is your page URL, minus
the domain name and protocol (e.g. http://livestream.com/). The widget does not
support streams hosted on new.livestream.com (at time of writing, Livestream
themselves does not support embedding from new.livestream.com). The widget is
configured to only support live streams, not any of the other functionality
offered by LiveStream, including recorded video.

2. Display name: This property is ignored by the Livestream widget (it uses the
actual name of your account, regardless of what you enter here), but is used to
display the name of the Livestream page in the hover text of the placeholder in
CKEditor. It is also used on the live page if JavaScript is disabled - in place
of the widget, a text link is displayed, showing "Livestream: " and then your
display name.
