/**
 * @file
 */

// Global variable used to store fakeImage element when socialmedia is double clicked.
var doubleclick_element;

CKEDITOR.plugins.add('socialmedia', {
  requires : ['dialog', 'fakeobjects'],
  init: function (editor) {
    // Define plugin name.
    var pluginName = 'socialmedia';

    // Set up object to share variables amongst scripts.
    CKEDITOR.socialmedia = {};

    // Register button for Twitter.
    editor.ui.addButton('Twitter', {
      label : "Add/Edit Twitter Feed",
      command : 'twitter',
      icon: this.path + 'icons/twitter.png',
    });
    // Register right-click menu item for Twitter.
    editor.addMenuItems({
      twitter : {
        label : "Edit Twitter Feed",
        icon: this.path + 'icons/twitter.png',
        command : 'twitter',
        group : 'image',
        order : 1
      }
    });

    // Configure DTD.
    CKEDITOR.dtd.cktwitter = {};
    // Define element as "block level" so the editor doesn't wrap it in 'p'.
    CKEDITOR.dtd.$block.cktwitter = 1;
    CKEDITOR.dtd.body.cktwitter = 1;
    // Make cktwitter to be a self-closing tag.
    CKEDITOR.dtd.$empty.cktwitter = 1;

    // Make sure the fake element for Twitter has a name.
    CKEDITOR.socialmedia.cktwitter = 'Twitter widget';
    CKEDITOR.lang.en.fakeobjects.cktwitter = CKEDITOR.socialmedia.cktwitter;
    // Add JavaScript file that defines the dialog box for Twitter.
    CKEDITOR.dialog.add('twitter', this.path + 'dialogs/twitter.js');
    // Register command to open dialog box when button is clicked.
    editor.addCommand('twitter', new CKEDITOR.dialogCommand('twitter'));
    // Regular expressions for Twitter.
    CKEDITOR.socialmedia.twitter_username_regex = /^[a-zA-Z0-9_]{1,15}$/;
    CKEDITOR.socialmedia.twitter_listname_regex = /^[a-zA-Z][a-zA-Z0-9_-]{0,24}$/;
    CKEDITOR.socialmedia.twitter_tweet_regex = /.*twitter\.com\/([^"\/]+\/status\/[0-9]+)([\s\S]+$|)/;
    CKEDITOR.socialmedia.twitter_url_regex = /^\w{1,15}\/timelines\/\d+$/;
    CKEDITOR.socialmedia.twitter_timelineinput_regex = /^[a-zA-Z0-9_\s-]{2,100}$/;
    CKEDITOR.socialmedia.twitter_moment_url_regex = /^\w{1,15}\/moments\/\d+$/;
    // Register button for Facebook.
    editor.ui.addButton('Facebook', {
      label : "Add/Edit Facebook Feed",
      command : 'facebook',
      icon: this.path + 'icons/facebook.png',
    });
    // Register right-click menu item for Facebook.
    editor.addMenuItems({
      facebook : {
        label : "Edit Facebook Feed",
        icon: this.path + 'icons/facebook.png',
        command : 'facebook',
        group : 'image',
        order : 1
      }
    });

    // Configure DTD.
    CKEDITOR.dtd.ckfacebook = {};
    // Define element as "block level" so the editor doesn't wrap it in 'p'.
    CKEDITOR.dtd.$block.ckfacebook = 1;
    CKEDITOR.dtd.body.ckfacebook = 1;
    // Make ckfacebook to be a self-closing tag.
    CKEDITOR.dtd.$empty.ckfacebook = 1;

    // Make sure the fake element for Facebook has a name.
    CKEDITOR.socialmedia.ckfacebook = 'Facebook widget';
    CKEDITOR.lang.en.fakeobjects.ckfacebook = CKEDITOR.socialmedia.ckfacebook;
    // Add JavaScript file that defines the dialog box for Facebook.
    CKEDITOR.dialog.add('facebook', this.path + 'dialogs/facebook.js');
    // Register command to open dialog box when button is clicked.
    editor.addCommand('facebook', new CKEDITOR.dialogCommand('facebook'));
    /*
    Facebook web addresses can only contain alphanumeric characters (a-z, 0-9) or periods ("."),
    and must contain at least one letter. You may also include periods and capital letters to
    make your web address easier to read, but their use by others in finding your Page is optional.
    For example, http://www.facebook.com/fbsitegovernance and http://www.facebook.com/FB.Site.
    Governance go to the same Page.
    Pages don't necessarily follow these rules. Some pages are /pages/pagename/number
    (e.g. www.facebook.com/pages/Student-Life-Centre/184707270634) *might* be a 75 character
    limit (we will assume so until proven otherwise).
    */
    CKEDITOR.socialmedia.facebook_username_regex_1 = /[a-zA-Z]/;
    CKEDITOR.socialmedia.facebook_username_regex_2 = /^(?:[-.a-zA-Z0-9]{1,75}|pages\/[-.a-zA-Z0-9]+\/[0-9]+)$/;
    CKEDITOR.socialmedia.facebook_url_regex_1 = /^https?:\/\/(?:www\.)?facebook\.com\/.+/;

    // Register button for Livestream.
    editor.ui.addButton('Livestream', {
      label : "Add/Edit Livestream Video",
      command : 'livestream',
      icon: this.path + 'icons/livestream.png',
    });
    // Register right-click menu item for Livestream.
    editor.addMenuItems({
      livestream : {
        label : "Edit Livestream Video",
        icon: this.path + 'icons/livestream.png',
        command : 'livestream',
        group : 'image',
        order : 1
      }
    });

    // Configure DTD.
    CKEDITOR.dtd.cklivestream = {};
    // Define element as "block level" so the editor doesn't wrap it in 'p'.
    CKEDITOR.dtd.$block.cklivestream = 1;
    CKEDITOR.dtd.body.cklivestream = 1;
    // Make cklivestream to be a self-closing tag.
    CKEDITOR.dtd.$empty.cklivestream = 1;

    // Make sure the fake element for Livestream has a name.
    CKEDITOR.socialmedia.cklivestream = 'Livestream video';
    CKEDITOR.lang.en.fakeobjects.cklivestream = CKEDITOR.socialmedia.cklivestream;
    // Add JavaScript file that defines the dialog box for Livestream.
    CKEDITOR.dialog.add('livestream', this.path + 'dialogs/livestream.js');
    // Register command to open dialog box when button is clicked.
    editor.addCommand('livestream', new CKEDITOR.dialogCommand('livestream'));
    // Regular expressions for Livestream.
    CKEDITOR.socialmedia.livestream_username_regex = /^[a-zA-Z0-9_\/]{4,40}$/;

    // Register button for Mailman Mailing Lists.
    editor.ui.addButton('Mailman', {
      label : "Add/Edit Mailman Subscription Form",
      command : 'mailman',
      icon: this.path + 'icons/mailinglists.png',
    });
    // Register right-click menu item for Livestream.
    editor.addMenuItems({
      mailman : {
        label : "Edit Mailman Subscription Form",
        icon: this.path + 'icons/mailinglists.png',
        command : 'mailman',
        group : 'image',
        order : 1
      }
    });

    // Configure DTD.
    CKEDITOR.dtd.ckmailman = {};
    // Define element as "block level" so the editor doesn't wrap it in 'p'.
    CKEDITOR.dtd.$block.ckmailman = 1;
    CKEDITOR.dtd.body.ckmailman = 1;
    // Make ckmailman to be a self-closing tag.
    CKEDITOR.dtd.$empty.ckmailman = 1;

    // Make sure the fake element for Mailman Mailing Lists has a name.
    CKEDITOR.socialmedia.ckmailman = 'Mailman subscription form';
    CKEDITOR.lang.en.fakeobjects.ckmailman = CKEDITOR.socialmedia.ckmailman;
    // Add JavaScript file that defines the dialog box for Mailman Mailing Lists.
    CKEDITOR.dialog.add('mailman', this.path + 'dialogs/mailman.js');
    // Register command to open dialog box when button is clicked.
    editor.addCommand('mailman', new CKEDITOR.dialogCommand('mailman'));
    // Regular expressions for Mailman Mailing Lists.
    /*
    jbg: pax: ACCEPTABLE_LISTNAME_CHARACTERS = '[-+_.=a-z0-9]' <- so "[-+_.=a-z0-9]*@lists\.uwaterloo\.ca"
    pax: jbg: no max/min length?
    jbg: pax, well email means min = 1, max=64...
    jbg: but we don't define anything specific on length..
    ...but http://ist.uwaterloo.ca/~kdpeck/mailman/Mailman.pdf says they can be 256 characters...
     */
    CKEDITOR.socialmedia.mailman_regex = /^[-+_.=a-z0-9]{1,256}$/;

    // Register button for Tableau.
    editor.ui.addButton('Tableau', {
      label : "Add/Edit Tableau Visualization",
      command : 'tableau',
      icon: this.path + 'icons/tableau.png',
    });
    // Register right-click menu item for Tableau.
    editor.addMenuItems({
      tableau : {
        label : "Edit Tableau Visualization",
        icon: this.path + 'icons/tableau.png',
        command : 'tableau',
        group : 'image',
        order : 1
      }
    });

    // Configure DTD.
    CKEDITOR.dtd.cktableau = {};
    // Define element as "block level" so the editor doesn't wrap it in 'p'.
    CKEDITOR.dtd.$block.cktableau = 1;
    CKEDITOR.dtd.body.cktableau = 1;
    // Make cktableau to be a self-closing tag.
    CKEDITOR.dtd.$empty.cktableau = 1;

    // Make sure the fake element for Tableau has a name.
    CKEDITOR.socialmedia.cktableau = 'Tableau visualization';
    CKEDITOR.lang.en.fakeobjects.cktableau = CKEDITOR.socialmedia.cktableau;
    // Add JavaScript file that defines the dialog box for Tableau.
    CKEDITOR.dialog.add('tableau', this.path + 'dialogs/tableau.js');
    // Register command to open dialog box when button is clicked.
    editor.addCommand('tableau', new CKEDITOR.dialogCommand('tableau'));

    // Register button for Tint.
    editor.ui.addButton('Tint', {
      label : "Add/Edit Tint Wall",
      command : 'tint',
      icon: this.path + 'icons/tint.png',
    });
    // Register right-click menu item for Tint.
    editor.addMenuItems({
      tint : {
        label : "Edit Tint Wall",
        icon: this.path + 'icons/tint.png',
        command : 'tint',
        group : 'image',
        order : 1
      }
    });

    // Configure DTD.
    CKEDITOR.dtd.cktint = {};
    // Define element as "block level" so the editor doesn't wrap it in 'p'.
    CKEDITOR.dtd.$block.cktint = 1;
    CKEDITOR.dtd.body.cktint = 1;
    // Make cktint to be a self-closing tag.
    CKEDITOR.dtd.$empty.cktint = 1;

    // Make sure the fake element for Tint has a name.
    CKEDITOR.socialmedia.cktint = 'Tint wall';
    CKEDITOR.lang.en.fakeobjects.cktint = CKEDITOR.socialmedia.cktint;
    // Add JavaScript file that defines the dialog box for Tint.
    CKEDITOR.dialog.add('tint', this.path + 'dialogs/tint.js');
    // Register command to open dialog box when button is clicked.
    editor.addCommand('tint', new CKEDITOR.dialogCommand('tint'));

    // Register button for Vimeo.
    editor.ui.addButton('Vimeo', {
      label : "Add/Edit Vimeo Video",
      command : 'vimeo',
      icon: this.path + 'icons/vimeo.png',
    });
    // Register right-click menu item for Vimeo.
    editor.addMenuItems({
      vimeo : {
        label : "Edit Vimeo Video",
        icon: this.path + 'icons/vimeo.png',
        command : 'vimeo',
        group : 'image',
        order : 1
      }
    });

    // Configure DTD.
    CKEDITOR.dtd.ckvimeo = {};
    // Define element as "block level" so the editor doesn't wrap it in 'p'.
    CKEDITOR.dtd.$block.ckvimeo = 1;
    CKEDITOR.dtd.body.ckvimeo = 1;
    // Make ckvimeo to be a self-closing tag.
    CKEDITOR.dtd.$empty.ckvimeo = 1;

    // Make sure the fake element for Vimeo has a name.
    CKEDITOR.socialmedia.ckvimeo = 'Vimeo video';
    CKEDITOR.lang.en.fakeobjects.ckvimeo = CKEDITOR.socialmedia.ckvimeo;
    // Add JavaScript file that defines the dialog box for Vimeo.
    CKEDITOR.dialog.add('vimeo', this.path + 'dialogs/vimeo.js');
    // Register command to open dialog box when button is clicked.
    editor.addCommand('vimeo', new CKEDITOR.dialogCommand('vimeo'));

    // Regular expressions for vimeo.
    CKEDITOR.socialmedia.vimeo_url_regex = /.*vimeo\.com\/([0-9]+)$/;

    // Register button for MailChimp.
    editor.ui.addButton('MailChimp', {
      label : "Add/Edit MailChimp",
      command : 'mailchimp',
      icon: this.path + 'icons/mailchimp.png',
    });
    // Register right-click menu item for MailChimp.
    editor.addMenuItems({
      mailchimp : {
        label : "Edit MailChimp",
        icon: this.path + 'icons/mailchimp.png',
        command : 'mailchimp',
        group : 'image',
        order : 1
      }
    });

    // Configure DTD.
    CKEDITOR.dtd.ckmailchimp = {};
    // Define element as "block level" so the editor doesn't wrap it in 'p'.
    CKEDITOR.dtd.$block.ckmailchimp = 1;
    CKEDITOR.dtd.body.ckmailchimp = 1;
    // Make ckmailchimp to be a self-closing tag.
    CKEDITOR.dtd.$empty.ckmailchimp = 1;

    // Make sure the fake element for MailChimp has a name.
    CKEDITOR.socialmedia.ckmailchimp = 'MailChimp';
    CKEDITOR.lang.en.fakeobjects.ckmailchimp = CKEDITOR.socialmedia.ckmailchimp;
    // Add JavaScript file that defines the dialog box for MailChimp.
    CKEDITOR.dialog.add('mailchimp', this.path + 'dialogs/mailchimp.js');
    // Register command to open dialog box when button is clicked.
    editor.addCommand('mailchimp', new CKEDITOR.dialogCommand('mailchimp'));

    // Register button for Hootsuite Campaigns.
    editor.ui.addButton('Hootsuite', {
      label : "Add/Edit Hootsuite Campaigns",
      command : 'hootsuite',
      icon: this.path + 'icons/hootsuite.png',
    });
    // Register right-click menu item for Hootsuite Campaigns.
    editor.addMenuItems({
      hootsuite : {
        label : "Edit Hootsuite Campaigns",
        icon: this.path + 'icons/hootsuite.png',
        command : 'hootsuite',
        group : 'image',
        order : 1
      }
    });

    // Configure DTD.
    CKEDITOR.dtd.ckhootsuite = {};
    // Define element as "block level" so the editor doesn't wrap it in 'p'.
    CKEDITOR.dtd.$block.ckhootsuite = 1;
    CKEDITOR.dtd.body.ckhootsuite = 1;
    // Make ckhootsuite to be a self-closing tag.
    CKEDITOR.dtd.$empty.ckhootsuite = 1;

    // Make sure the fake element for Hootsuite Campaigns has a name.
    CKEDITOR.socialmedia.ckhootsuite = 'Hootsuite Campaigns';
    CKEDITOR.lang.en.fakeobjects.ckhootsuite = CKEDITOR.socialmedia.ckhootsuite;
    // Add JavaScript file that defines the dialog box for Hootsuite Campaigns.
    CKEDITOR.dialog.add('hootsuite', this.path + 'dialogs/hootsuite.js');
    // Register command to open dialog box when button is clicked.
    editor.addCommand('hootsuite', new CKEDITOR.dialogCommand('hootsuite'));
    CKEDITOR.socialmedia.hootsuite_regex = /^[a-zA-Z0-9_-]+$/;

    // Register button for CodePen.
    editor.ui.addButton('CodePen', {
      label : "Add/Edit CodePen embed",
      command : 'codepen',
      icon: this.path + 'icons/codepen.png',
    });
    // Register right-click menu item for CodePen.
    editor.addMenuItems({
      codepen : {
        label : "Edit CodePen embed",
        icon: this.path + 'icons/codepen.png',
        command : 'codepen',
        group : 'image',
        order : 1
      }
    });

     // Configure DTD.
    CKEDITOR.dtd.ckcodepen = {};
    // Define element as "block level" so the editor doesn't wrap it in 'p'.
    CKEDITOR.dtd.$block.ckcodepen = 1;
    CKEDITOR.dtd.body.ckcodepen = 1;
    // Make ckcodepen to be a self-closing tag.
    CKEDITOR.dtd.$empty.ckcodepen = 1;

    // Make sure the fake element for CodePen has a name.
    CKEDITOR.socialmedia.ckcodepen = 'CodePen embed';
    CKEDITOR.lang.en.fakeobjects.ckcodepen = CKEDITOR.socialmedia.ckcodepen;
    // Add JavaScript file that defines the dialog box for CodePen.
    CKEDITOR.dialog.add('codepen', this.path + 'dialogs/codepen.js');
    // Register command to open dialog box when button is clicked.
    editor.addCommand('codepen', new CKEDITOR.dialogCommand('codepen'));
    // CodePen usernames can only contain letters, numbers, and dashes.
    // CodePen ID is assumed to always be 6 or 7 upper- or lower-case letters.
    // Display name and title can be anything.
    CKEDITOR.socialmedia.codepen_username_regex = /^[a-zA-Z0-9_-]+$/;
    CKEDITOR.socialmedia.codepen_id_regex = /^[a-zA-Z]{6,7}$/;

    // Register button for Social Intents.
    editor.ui.addButton('Social Intents', {
      label : "Add/Edit Social Intents embed",
      command : 'socialintents',
      icon: this.path + 'icons/socialintents.png',
    });
    // Register right-click menu item for Social Intents.
    editor.addMenuItems({
      socialintents : {
        label : "Edit Social Intents embed",
        icon: this.path + 'icons/socialintents.png',
        command : 'socialintents',
        group : 'image',
        order : 1
      }
    });

    // Configure DTD.
    CKEDITOR.dtd.cksocialintents = {};
    // Define element as "block level" so the editor doesn't wrap it in 'p'.
    CKEDITOR.dtd.$block.cksocialintents = 1;
    CKEDITOR.dtd.body.cksocialintents = 1;
    // Make cksocialintents to be a self-closing tag.
    CKEDITOR.dtd.$empty.cksocialintents = 1;

    // Make sure the fake element for Social Intents has a name.
    CKEDITOR.socialmedia.cksocialintents = 'Social Intents embed';
    CKEDITOR.lang.en.fakeobjects.cksocialintents = CKEDITOR.socialmedia.cksocialintents;
    // Add JavaScript file that defines the dialog box for Social Intents.
    CKEDITOR.dialog.add('socialintents', this.path + 'dialogs/socialintents.js');
    // Register command to open dialog box when button is clicked.
    editor.addCommand('socialintents', new CKEDITOR.dialogCommand('socialintents'));
    // For now we are assuming Social Intents usernames can only contain letters, numbers, and dashes.
    CKEDITOR.socialmedia.socialintents_username_regex = /^[a-zA-Z0-9_-]+$/;

    // Open the appropriate dialog box if an element is double-clicked.
    editor.on('doubleclick', function (evt) {
      var element = evt.data.element;

      // Store the element as global variable to be used in onShow of dialog, when double clicked.
      doubleclick_element = element;

      if (element.is('img') && element.data('cke-real-element-type') === 'cktwitter') {
        // alert("Unfortunately, double-clicking widgets to edit them is not available in this release.\n\nPlease right click and select 'Edit Twitter Feed' from the menu, or click on the Twitter icon in the toolbar.");.
        evt.data.dialog = 'twitter';
      }
      else if (element.is('img') && element.data('cke-real-element-type') === 'ckfacebook') {
        // alert("Unfortunately, double-clicking widgets to edit them is not available in this release.\n\nPlease right click and select 'Edit Facebook Feed' from the menu, or click on the Facebook icon in the toolbar.");.
        evt.data.dialog = 'facebook';
      }
      else if (element.is('img') && element.data('cke-real-element-type') === 'cklivestream') {
        // alert("Unfortunately, double-clicking widgets to edit them is not available in this release.\n\nPlease right click and select 'Edit Livestream Video' from the menu, or click on the Livestream icon in the toolbar.");.
        evt.data.dialog = 'livestream';
      }
      else if (element.is('img') && element.data('cke-real-element-type') === 'ckmailman') {
        // alert("Unfortunately, double-clicking widgets to edit them is not available in this release.\n\nPlease right click and select 'Edit Mailman Subscription Form' from the menu, or click on the Mailman icon in the toolbar.");.
        evt.data.dialog = 'mailman';
      }
      else if (element.is('img') && element.data('cke-real-element-type') === 'cktableau') {
        // alert("Unfortunately, double-clicking widgets to edit them is not available in this release.\n\nPlease right click and select 'Edit Tableau Visualization' from the menu, or click on the Tableau icon in the toolbar.");.
        evt.data.dialog = 'tableau';
      }
      else if (element.is('img') && element.data('cke-real-element-type') === 'cktint') {
        // alert("Unfortunately, double-clicking widgets to edit them is not available in this release.\n\nPlease right click and select 'Edit Tint Wall' from the menu, or click on the Tint icon in the toolbar.");.
        evt.data.dialog = 'tint';
      }
      else if (element.is('img') && element.data('cke-real-element-type') === 'ckvimeo') {
        // alert("Unfortunately, double-clicking widgets to edit them is not available in this release.\n\nPlease right click and select 'Edit Vimeo Video' from the menu, or click on the Vimeo icon in the toolbar.");.
        evt.data.dialog = 'vimeo';
      }
      else if (element.is('img') && element.data('cke-real-element-type') === 'ckmailchimp') {
        // alert("Unfortunately, double-clicking widgets to edit them is not available in this release.\n\nPlease right click and select 'Edit MailChimp' from the menu, or click on the MailChimp icon in the toolbar.");.
        evt.data.dialog = 'mailchimp';
      }
      else if (element.is('img') && element.data('cke-real-element-type') === 'ckhootsuite') {
        evt.data.dialog = 'hootsuite';
      }
      else if (element.is('img') && element.data('cke-real-element-type') === 'ckcodepen') {
        evt.data.dialog = 'codepen';
      }
      else if (element.is('img') && element.data('cke-real-element-type') === 'cksocialintents') {
        evt.data.dialog = 'socialintents';
      }
    });

    // Add the appropriate right-click menu item if an element is right-clicked.
    if (editor.contextMenu) {
      editor.contextMenu.addListener(function (element, selection) {
        if (element && element.is('img') && element.data('cke-real-element-type') === 'cktwitter') {
          return { twitter : CKEDITOR.TRISTATE_OFF };
        }
        else if (element && element.is('img') && element.data('cke-real-element-type') === 'ckfacebook') {
          return { facebook : CKEDITOR.TRISTATE_OFF };
        }
        else if (element && element.is('img') && element.data('cke-real-element-type') === 'cklivestream') {
          return { livestream : CKEDITOR.TRISTATE_OFF };
        }
        else if (element && element.is('img') && element.data('cke-real-element-type') === 'ckmailman') {
          return { mailman : CKEDITOR.TRISTATE_OFF };
        }
        else if (element && element.is('img') && element.data('cke-real-element-type') === 'cktableau') {
          return { tableau : CKEDITOR.TRISTATE_OFF };
        }
        else if (element && element.is('img') && element.data('cke-real-element-type') === 'cktint') {
          return { tint : CKEDITOR.TRISTATE_OFF };
        }
        else if (element && element.is('img') && element.data('cke-real-element-type') === 'ckvimeo') {
          return { vimeo : CKEDITOR.TRISTATE_OFF };
        }
        else if (element && element.is('img') && element.data('cke-real-element-type') === 'ckmailchimp') {
          return { mailchimp : CKEDITOR.TRISTATE_OFF };
        }
        else if (element && element.is('img') && element.data('cke-real-element-type') === 'ckhootsuite') {
          return { hootsuite : CKEDITOR.TRISTATE_OFF };
        }
        else if (element && element.is('img') && element.data('cke-real-element-type') === 'ckcodepen') {
          return { codepen : CKEDITOR.TRISTATE_OFF };
        }
        else if (element && element.is('img') && element.data('cke-real-element-type') === 'cksocialintents') {
          return { socialintents : CKEDITOR.TRISTATE_OFF };
        }
      });
    }

    // Add CSS to use in-editor to style custom (fake) elements.
    CKEDITOR.addCss(
      'img.cktwitter {' +
        'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/twitter.png') + ');' +
        'background-position: center center;' +
        'background-repeat: no-repeat;' +
        'background-color: #000;' +
        'width: 100%;' +
        'height: 350px;' +
        'margin: 0 0 10px 0;' +
        'margin-left: auto;' +
        'margin-right: auto;' +
      '}' +

      'img.ckfacebook {' +
        'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/facebook.png') + ');' +
        'background-position: center center;' +
        'background-repeat: no-repeat;' +
        'background-color: #000;' +
        'width: 100%;' +
        'height: 350px;' +
        'display: block;' +
        'clear: both;' +
        'margin: 0 0 10px 0;' +
      '}' +

      'img.cklivestream {' +
        'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/livestream.png') + ');' +
        'background-position: center center;' +
        'background-repeat: no-repeat;' +
        'background-color: #000;' +
        'width: 100%;' +
        'height: 100%;' +
        'margin: 0 0 10px 0;' +
        'margin-left: auto;' +
        'margin-right: auto;' +
      '}' +

      // Livestream: Definitions for wide width.
      '.uw_tf_standard_wide img.cklivestream {' +
        'max-height: 460px;' +
      '}' +
      '.uw_tf_standard_wide .col-50 img.cklivestream {' +
        'max-height: 223px;' +
      '}' +
      '.uw_tf_standard_wide .col-33 img.cklivestream {' +
        'max-height: 147px;' +
      '}' +
      '.uw_tf_standard_wide .col-66 img.cklivestream {' +
        'max-height: 297px;' +
      '}' +
      '.uw_tf_standard_wide .threecol-33 img.cklivestream {' +
        'max-height: 147px;' +
      '}' +

      // Livestream: Definitions for standard width.
      '.uw_tf_standard img.cklivestream {' +
        'max-height: 307px;' +
      '}' +
      '.uw_tf_standard .col-50 img.cklivestream {' +
        'max-height: 146px;' +
      '}' +
      '.uw_tf_standard .col-33 img.cklivestream {' +
        'max-height: 100px;' +
      '}' +
      '.uw_tf_standard .col-66 img.cklivestream {' +
        'max-height: 196px;' +
      '}' +
      '.uw_tf_standard .threecol-33 img.cklivestream {' +
        'max-height: 100px;' +
      '}' +

      'img.ckmailman {' +
        'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/mailinglists.png') + ');' +
        'background-position: center center;' +
        'background-repeat: no-repeat;' +
        'background-color: #000;' +
        'width: 100%;' +
        'height: 50px;' +
        'margin: 0 0 10px 0;' +
        'margin-left: auto;' +
        'margin-right: auto;' +
      '}' +

      'img.cktableau {' +
        'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/tableau.png') + ');' +
        'background-position: center center;' +
        'background-repeat: no-repeat;' +
        'background-color: #000;' +
        'width: 100%;' +
        'display: block;' +
        'clear: both;' +
        'margin: 0 0 10px 0;' +
      '}' +

      'img.cktint {' +
        'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/tint.png') + ');' +
        'background-position: center center;' +
        'background-repeat: no-repeat;' +
        'background-color: #000;' +
        'width: 100%;' +
        'height: 250px;' +
        'display: block;' +
        'clear: both;' +
        'margin: 0 0 10px 0;' +
      '}' +

      'img.ckvimeo {' +
        'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/vimeo.png') + ');' +
        'background-position: center center;' +
        'background-repeat: no-repeat;' +
        'background-color: #000;' +
        'width: 100%;' +
        'height: 100%;' +
        'margin: 0 0 10px 0;' +
        'margin-left: auto;' +
        'margin-right: auto;' +
      '}' +

      'img.ckmailchimp {' +
      'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/mailchimp.png') + ');' +
      'background-position: center center;' +
      'background-repeat: no-repeat;' +
      'background-color: #000;' +
      'width: 100%;' +
      'height: 50px;' +
      'margin: 0 0 10px 0;' +
      'margin-left: auto;' +
      'margin-right: auto;' +
    '}' +

    'img.ckhootsuite {' +
      'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/hootsuite.png') + ');' +
      'background-position: center center;' +
      'background-repeat: no-repeat;' +
      'background-color: #000;' +
      'width: 100%;' +
      'height: 50px;' +
      'margin: 0 0 10px 0;' +
      'margin-left: auto;' +
      'margin-right: auto;' +
    '}' +

      'img.ckcodepen {' +
        'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/codepen.png') + ');' +
        'background-position: center center;' +
        'background-repeat: no-repeat;' +
        'background-color: #000;' +
        'width: 100%;' +
        'height: 50px;' +
        'margin: 0 0 10px 0;' +
        'margin-left: auto;' +
        'margin-right: auto;' +
      '}' +

      'img.cksocialintents {' +
        'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/socialintents.png') + ');' +
        'background-position: center center;' +
        'background-repeat: no-repeat;' +
        'background-color: #000;' +
        'width: 100%;' +
        'height: 50px;' +
        'margin: 0 0 10px 0;' +
        'margin-left: auto;' +
        'margin-right: auto;' +
      '}' +

       // Facebook: Definitions for wide width.
      '.uw_tf_standard_wide p img.ckfacebook {' +
        'height: 350px;' +
      '}' +

      // Facebook: Definitions for standard width.
      '.uw_tf_standard p img.ckfacebook {' +
        'height: 350px;' +
      '}' +

      // Twitter: Definitions for wide width.
      '.uw_tf_standard_wide p img.cktwitter {' +
        'height: 350px;' +
      '}' +

      // Twitter: Definitions for standard width.
      '.uw_tf_standard p img.cktwitter {' +
        'height: 350px;' +
      '}' +

      // Mailman: Definitions for wide width.
      '.uw_tf_standard_wide p img.ckmailman {' +
        'height: 50px;' +
      '}' +

      // Mailman: Definitions for standard width.
      '.uw_tf_standard p img.ckmailman {' +
        'height: 50px;' +
      '}' +

      // Vimeo: Definitions for wide width.
      '.uw_tf_standard_wide img.ckvimeo {' +
        'max-height: 383px;' +
      '}' +
      '.uw_tf_standard_wide .col-50 img.ckvimeo {' +
        'max-height: 184px;' +
      '}' +
      '.uw_tf_standard_wide .col-33 img.ckvimeo {' +
        'max-height: 123px;' +
      '}' +
      '.uw_tf_standard_wide .col-66 img.ckvimeo {' +
        'max-height: 245px;' +
      '}' +
      '.uw_tf_standard_wide .threecol-33 img.ckvimeo {' +
        'max-height: 120px;' +
      '}' +

      // Vimeo: Definitions for standard width.
      '.uw_tf_standard img.ckvimeo {' +
        'max-height: 250px;' +
      '}' +
      '.uw_tf_standard .col-50 img.ckvimeo {' +
        'max-height: 123px;' +
      '}' +
      '.uw_tf_standard .col-33 img.ckvimeo {' +
        'max-height: 110px;' +
      '}' +
      '.uw_tf_standard .col-66 img.ckvimeo {' +
        'max-height: 160px;' +
      '}' +
      '.uw_tf_standard .threecol-33 img.ckvimeo {' +
        'max-height: 80px;' +
      '}' +

      // MailChimp: Definitions for wide width.
      '.uw_tf_standard_wide p img.ckmailchimp {' +
        'height: 50px;' +
      '}' +

      // MailChimp: Definitions for standard width.
      '.uw_tf_standard p img.ckmailchimp {' +
        'height: 50px;' +
      '}'
    );
  },
  afterInit : function (editor) {
    // Make fake image display on first load/return from source view.
    if (editor.dataProcessor.dataFilter) {
      editor.dataProcessor.dataFilter.addRules({
        elements : {
          cktwitter : function (element) {
            // Reset title.
            CKEDITOR.lang.en.fakeobjects.cktwitter = CKEDITOR.socialmedia.cktwitter;
            twittertype = element.attributes['data-type'];
            widgettext = 'Twitter widget: ';
            switch (twittertype) {
              case 'profile':
                widgettext += 'Tweets by @' + element.attributes['data-username'];
                break;

              case 'faves':
                widgettext += 'Favourite Tweets by @' + element.attributes['data-username'];
                break;

              case 'list':
                widgettext += 'Tweets from @' + element.attributes['data-username'] + '/' + element.attributes['data-listname'];
                break;

              case 'tweet':
                widgettext += 'Embedded Tweet';
                break;

              case 'custom':
                widgettext += 'Collection timeline "' + element.attributes['data-timeline'] + '"';
                break;

              case 'grid':
                widgettext += 'Collection grid "' + element.attributes['data-timeline'] + '"';
                break;

              case 'moment':
                widgettext += 'Moment';
                break;
            }

            CKEDITOR.lang.en.fakeobjects.cktwitter = widgettext;
            // Note that this just accepts whatever attributes are on the element; may want to filter these.
            return editor.createFakeParserElement(element, 'cktwitter', 'cktwitter', false);
          },
          ckfacebook : function (element) {
            // Reset title.
            CKEDITOR.lang.en.fakeobjects.ckfacebook = CKEDITOR.socialmedia.ckfacebook;
            // Adjust title if a display name is present.
            if (element.attributes['data-type'] == "singlepost") {
              CKEDITOR.lang.en.fakeobjects.ckfacebook = 'Facebook post: ' + element.attributes['data-displayname'];
            }
            else {
              // Anything that isn't a single post is assumed to be a timeline.
              // This allows widgets created before we supported anything else to work.
              CKEDITOR.lang.en.fakeobjects.ckfacebook = 'Facebook timeline: ' + element.attributes['data-displayname'];
            }
            // Note that this just accepts whatever attributes are on the element; may want to filter these.
            return editor.createFakeParserElement(element, 'ckfacebook', 'ckfacebook', false);
          },
          cklivestream : function (element) {
            // Reset title.
            CKEDITOR.lang.en.fakeobjects.cklivestream = CKEDITOR.socialmedia.cklivestream;
            // Adjust title if a display name is present.
            if (element.attributes['data-displayname']) {
              CKEDITOR.lang.en.fakeobjects.cklivestream += ': ' + element.attributes['data-displayname'];
            }
            // Note that this just accepts whatever attributes are on the element; may want to filter these.
            return editor.createFakeParserElement(element, 'cklivestream', 'cklivestream', false);
          },
          ckmailman : function (element) {
            // Reset title.
            CKEDITOR.lang.en.fakeobjects.ckmailman = CKEDITOR.socialmedia.ckmailman;
            // Adjust title if a list name is present.
            if (element.attributes['data-listname']) {
              CKEDITOR.lang.en.fakeobjects.ckmailman += ': ';
              if (element.attributes['data-servername']) {
                CKEDITOR.lang.en.fakeobjects.ckmailman += element.attributes['data-servername'] + '/';
              }
              CKEDITOR.lang.en.fakeobjects.ckmailman += element.attributes['data-listname'];
            }
            // Note that this just accepts whatever attributes are on the element; may want to filter these.
            return editor.createFakeParserElement(element, 'ckmailman', 'ckmailman', false);
          },
          cktableau : function (element) {
            // Reset title.
            CKEDITOR.lang.en.fakeobjects.cktableau = CKEDITOR.socialmedia.cktableau;
            // Adjust title if a list name is present.
            if (element.attributes['data-url']) {
              CKEDITOR.lang.en.fakeobjects.cktableau += ': ' + element.attributes['data-url'];
            }
            // Note that this just accepts whatever attributes are on the element; may want to filter these.
            tableau_return = editor.createFakeParserElement(element, 'cktableau', 'cktableau', false);
            // Set the fake object to the entered height; if there isn't one, use 100 so it's not invisible.
            if (!element.attributes['data-height']) {
              element.attributes['data-height'] = '100';
            }
            tableau_return.attributes.height = element.attributes['data-height'];
            return tableau_return;
          },
          cktint : function (element) {
            // Reset title.
            CKEDITOR.lang.en.fakeobjects.cktint = CKEDITOR.socialmedia.cktint;
            // Adjust title if a list name is present.
            if (element.attributes['data-id']) {
              CKEDITOR.lang.en.fakeobjects.cktint += ': ' + element.attributes['data-id'];
            }
            // Note that this just accepts whatever attributes are on the element; may want to filter these.
            tint_return = editor.createFakeParserElement(element, 'cktint', 'cktint', false);
            // Set the fake object to the entered height; if there isn't one, use 100 so it's not invisible.
            if (!element.attributes['data-height']) {
              element.attributes['data-height'] = '100';
            }
            tint_return.attributes.height = element.attributes['data-height'];
            return tint_return;
          },
          ckvimeo : function (element) {
            // Reset title.
            CKEDITOR.lang.en.fakeobjects.ckvimeo = CKEDITOR.socialmedia.ckvimeo;
            // Adjust title if a list name is present.
            if (element.attributes['data-url']) {
              CKEDITOR.lang.en.fakeobjects.ckvimeo += ': ' + element.attributes['data-url'];
            }
            // Note that this just accepts whatever attributes are on the element; may want to filter these.
            return editor.createFakeParserElement(element, 'ckvimeo', 'ckvimeo', false);
          },
          ckmailchimp : function (element) {
            // Reset title.
            CKEDITOR.lang.en.fakeobjects.ckmailchimp = CKEDITOR.socialmedia.ckmailchimp;
            // Adjust title if a list name is present.
            if (element.attributes['data-sourcecode']) {
              CKEDITOR.lang.en.fakeobjects.ckmailchimp += ': ';
              if (element.attributes['data-sourcecode']) {
                CKEDITOR.lang.en.fakeobjects.ckmailchimp += element.attributes['data-sourcecode'] + '/';
              }
              CKEDITOR.lang.en.fakeobjects.ckmailchimp += element.attributes['data-sourcecode'];
            }
            // Note that this just accepts whatever attributes are on the element; may want to filter these.
            return editor.createFakeParserElement(element, 'ckmailchimp', 'ckmailchimp', false);
          },
          ckhootsuite : function (element) {
            // Reset title.
            CKEDITOR.lang.en.fakeobjects.ckhootsuite = CKEDITOR.socialmedia.ckhootsuite;
            // Adjust title if a subdomain name is present.
            if (element.attributes['data-subdomain']) {
              CKEDITOR.lang.en.fakeobjects.ckhootsuite += ': ';
              CKEDITOR.lang.en.fakeobjects.ckhootsuite += element.attributes['data-subdomain'];
            }
            // Note that this just accepts whatever attributes are on the element; may want to filter these.
            return editor.createFakeParserElement(element, 'ckhootsuite', 'ckhootsuite', false);
          },
          ckcodepen : function (element) {
            // Reset title.
            CKEDITOR.lang.en.fakeobjects.ckcodepen = CKEDITOR.socialmedia.ckcodepen;
            CKEDITOR.lang.en.fakeobjects.ckcodepen += ': ';
            CKEDITOR.lang.en.fakeobjects.ckcodepen += element.attributes['data-title'];
            // Note that this just accepts whatever attributes are on the element; may want to filter these.
            return editor.createFakeParserElement(element, 'ckcodepen', 'ckcodepen', false);
          },
          cksocialintents : function (element) {
            // Reset title.
            CKEDITOR.lang.en.fakeobjects.cksocialintents = CKEDITOR.socialmedia.cksocialintents;
            CKEDITOR.lang.en.fakeobjects.cksocialintents += ': ';
            CKEDITOR.lang.en.fakeobjects.cksocialintents += element.attributes['data-username'];
            // Note that this just accepts whatever attributes are on the element; may want to filter these.
            return editor.createFakeParserElement(element, 'cksocialintents', 'cksocialintents', false);
          }
        }
      });
    }
  }
});
