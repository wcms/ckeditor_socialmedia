/**
 * @file
 */

(function () {
  // Variable used to track when the form is submitted.
  // i.e to check for double submissions on using enter key in dialog.
  var formSubmitted = false;
  var mailchimpDialog = function (editor) {
    return {
      title : 'MailChimp properties',
      minWidth : 625,
      minHeight : 150,
      contents: [{
        id: 'mailchimp',
        label: 'mailchimp',
        elements:[{
          type: 'textarea',
          rows: '5',
          id: 'mailchimpInput',
          label: 'Please paste MailChimp source code:',
          required: true,
          setup: function (element) {
                   this.setValue(element.getAttribute('data-sourcecode'));
          }
        }]
      }],
      onOk: function () {
        // Get form information.
        mailchimpInput = this.getValueOf('mailchimp','mailchimpInput');
        // Validate input. Note that there is probably a CKEditor specific way to do this, but this works.
        errors = "";
        if (!mailchimpInput) {
          errors = "You must copy and paste.\r\n";
        }

        // If form has been submitted before then set it back to not being seeing before.
        // i.e if this is double submission set it back to not being run before.
        if (formSubmitted == true) {
          formSubmitted = false;
          return false;
        }
        // Only display erros if there are errors to display and the form has not been run before.
        else if (errors && formSubmitted == false) {
          alert(errors);
          formSubmitted = true;
          return false;
        }
        else {
          // Create the mailchimp element.
          var ckmailchimpNode = new CKEDITOR.dom.element('ckmailchimp');
          // Save contents of dialog as attributes of the element.
          ckmailchimpNode.setAttribute('data-sourcecode', mailchimpInput);
          // Adjust title based on user input.
          CKEDITOR.lang.en.fakeobjects.ckmailchimp = CKEDITOR.socialmedia.ckmailchimp + ': ' + mailchimpInput;
          // Create the fake image for this element and insert into the document (realElement, className, realElementType, isResizable).
          var newFakeImage = editor.createFakeElement(ckmailchimpNode, 'ckmailchimp', 'ckmailchimp', false);
          // Set the fake object to the entered height; if there isn't one, use 100 so it's not invisible.
          newFakeImage.addClass('ckmailchimp');
          if (this.fakeImage) {
            newFakeImage.replace(this.fakeImage);
            editor.getSelection().selectElement(newFakeImage);
          }
          else {
            editor.insertElement(newFakeImage);
          }
          // Reset title.
          CKEDITOR.lang.en.fakeobjects.ckmailchimp = CKEDITOR.socialmedia.ckmailchimp;
        }
      },
      onShow: function () {
        // Set up to handle existing items.
        this.fakeImage = this.ckmailchimpNode = null;
        var fakeImage;

        // Check if element is right clicked or icon was clicked, if not use global varaible doubleclick_element.
        if (this.getSelectedElement()) {
          fakeImage = this.getSelectedElement();
        }
        else {
          fakeImage = fakeImage;
        }

        if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'ckmailchimp') {
          this.fakeImage = fakeImage;
          var ckmailchimpNode = editor.restoreRealElement(fakeImage);
          this.ckmailchimpNode = ckmailchimpNode;
          this.setupContent(ckmailchimpNode);
        }
      }
    }
  }

  CKEDITOR.dialog.add('mailchimp', function (editor) {
    return mailchimpDialog(editor);
  });
})();
