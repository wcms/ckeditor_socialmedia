/**
 * @file
 */

(function () {
  // Variable used to track when the form is submitted.
  // i.e to check for double submissions on using enter key in dialog.
  var formSubmitted = false;
  var mailmanDialog = function (editor) {
    return {
      title : 'Mailman Mailing List Properties',
      minWidth : 625,
      minHeight : 100,
      contents: [{
        id: 'mailman',
        label: 'mailman',
        elements:[{
          type: 'select',
          id: 'mailmanserverInput',
          label: 'Mailman server:',
          items: [
                   ['lists.uwaterloo.ca (a.k.a. mailman.uwaterloo.ca)', 'lists.uwaterloo.ca'],
                   ['artsservices.uwaterloo.ca', 'artsservices.uwaterloo.ca'],
                   ['civmail.uwaterloo.ca', 'civmail.uwaterloo.ca'],
                   ['engmail.uwaterloo.ca', 'engmail.uwaterloo.ca'],
                   ['sciborg.uwaterloo.ca', 'sciborg.uwaterloo.ca']
                 ],
          required: true,
          setup: function (element) {
                   this.setValue(element.getAttribute('data-servername'));
          }
        },{
          type: 'text',
          id: 'mailmanInput',
          label: '<strong>Mailman mailing list subscription URL:</strong> https://&lt;servername&gt;/mailman/subscribe/',
          setup: function (element) {
                   this.setValue(element.getAttribute('data-listname'));
          }
        }]
      }],
      onOk: function () {
        // Get form information.
        mailmanInput = this.getValueOf('mailman','mailmanInput');
        mailmanserverInput = this.getValueOf('mailman','mailmanserverInput');
        // Validate input. Note that there is probably a CKEditor specific way to do this, but this works.
        errors = '';
        if (!CKEDITOR.socialmedia.mailman_regex.test(mailmanInput)) {
          errors += "You must enter a valid Mailman mailing list subscription URL.\r\n";
        }
        // If form has been submitted before then set it back to not being seeing before.
        // i.e if this is double submission set it back to not being run before.
        if (formSubmitted == true) {
          formSubmitted = false;
          return false;
        }
        // Only display erros if there are errors to display and the form has not been run before.
        else if (errors && formSubmitted == false) {
          alert(errors);
          formSubmitted = true;
          return false;
        }
        else {
          // Create the ckmailman element.
          var ckmailmanNode = new CKEDITOR.dom.element('ckmailman');
          // Save contents of dialog as attributes of the element.
          ckmailmanNode.setAttribute('data-listname',mailmanInput);
          ckmailmanNode.setAttribute('data-servername',mailmanserverInput);
          // Adjust title based on user input.
          CKEDITOR.lang.en.fakeobjects.ckmailman = CKEDITOR.socialmedia.ckmailman + ': ' + mailmanserverInput + '/' + mailmanInput;
          // Create the fake image for this element and insert into the document (realElement, className, realElementType, isResizable).
          var newFakeImage = editor.createFakeElement(ckmailmanNode, 'ckmailman', 'ckmailman', false);
          newFakeImage.addClass('ckmailman');
          if (this.fakeImage) {
            newFakeImage.replace(this.fakeImage);
            editor.getSelection().selectElement(newFakeImage);
          }
          else {
            editor.insertElement(newFakeImage);
          }
          // Reset title.
          CKEDITOR.lang.en.fakeobjects.ckmailman = CKEDITOR.socialmedia.ckmailman;
        }
      },
      onShow: function () {
        // Set up to handle existing items.
        this.fakeImage = this.ckmailmanNode = null;
        var fakeImage;

        // Check if element is right clicked or icon was clicked, if not use global varaible doubleclick_element.
        if (this.getSelectedElement()) {
          fakeImage = this.getSelectedElement();
        }
        else {
          fakeImage = doubleclick_element;
        }

        if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'ckmailman') {
          this.fakeImage = fakeImage;
          var ckmailmanNode = editor.restoreRealElement(fakeImage);
          this.ckmailmanNode = ckmailmanNode;
          this.setupContent(ckmailmanNode);
        }
      }
    }
  }

  CKEDITOR.dialog.add('mailman', function (editor) {
    return mailmanDialog(editor);
  });
})();
