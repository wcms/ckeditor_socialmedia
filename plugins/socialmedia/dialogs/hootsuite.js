/**
 * @file
 */

(function () {
  // Variable used to track when the form is submitted.
  // i.e to check for double submissions on using enter key in dialog.
  var formSubmitted = false;
  var hootsuiteDialog = function (editor) {
    return {
      title : 'Hootsuite Campaigns Properties',
      minWidth : 625,
      minHeight : 100,
      contents: [{
        id: 'hootsuite',
        label: 'hootsuite',
        elements:[{
          type: 'text',
          id: 'hootsuiteInput',
          label: '<strong>Hootsuite Campaigns subdomain:</strong> https://&lt;subdomain&gt;.hscampaigns.com (required)',
          setup: function (element) {
                   this.setValue(element.getAttribute('data-subdomain'));
          }
        },{
          type: 'text',
          id: 'heightInput',
          label: 'Display height in pixels (minimum 100): (required)',
          setup: function (element) {
                   this.setValue(element.getAttribute('data-height'));
          }
        }]
      }],
      onOk: function () {
        // Get form information.
        hootsuiteInput = this.getValueOf('hootsuite','hootsuiteInput');
        heightInput = this.getValueOf('hootsuite','heightInput');
        // Validate input. Note that there is probably a CKEditor specific way to do this, but this works.
        errors = '';
        if (!CKEDITOR.socialmedia.hootsuite_regex.test(hootsuiteInput)) {
          errors += "You must enter a valid Hootsuite Campaigns subdomain.\r\n";
        }
        if (!heightInput || heightInput.NaN || heightInput < 100 || Math.floor(heightInput) != heightInput) {
          errors += "You must enter a valid whole number for the height.\r\n";
        }

        // If form has been submitted before then set it back to not being seeing before.
        // i.e if this is double submission set it back to not being run before.
        if (formSubmitted == true) {
          formSubmitted = false;
          return false;
        }
        // Only display erros if there are errors to display and the form has not been run before.
        else if (errors && formSubmitted == false) {
          alert(errors);
          formSubmitted = true;
          return false;
        }
        else {
          // Create the ckhootsuite element.
          var ckhootsuiteNode = new CKEDITOR.dom.element('ckhootsuite');
          // Save contents of dialog as attributes of the element.
          ckhootsuiteNode.setAttribute('data-subdomain',hootsuiteInput);
          ckhootsuiteNode.setAttribute('data-height',heightInput);
          // Adjust title based on user input.
          CKEDITOR.lang.en.fakeobjects.ckhootsuite = CKEDITOR.socialmedia.ckhootsuite + ': ' + hootsuiteInput;
          // Create the fake image for this element and insert into the document (realElement, className, realElementType, isResizable).
          var newFakeImage = editor.createFakeElement(ckhootsuiteNode, 'ckhootsuite', 'ckhootsuite', false);
          newFakeImage.addClass('ckhootsuite');
          if (this.fakeImage) {
            newFakeImage.replace(this.fakeImage);
            editor.getSelection().selectElement(newFakeImage);
          }
          else {
            editor.insertElement(newFakeImage);
          }
          // Reset title.
          CKEDITOR.lang.en.fakeobjects.ckhootsuite = CKEDITOR.socialmedia.ckhootsuite;
        }
      },
      onShow: function () {
        // Set up to handle existing items.
        this.fakeImage = this.ckhootsuiteNode = null;
        var fakeImage;

        // Check if element is right clicked or icon was clicked, if not use global varaible doubleclick_element.
        if (this.getSelectedElement()) {
          fakeImage = this.getSelectedElement();
        }
        else {
          fakeImage = doubleclick_element;
        }

        if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'ckhootsuite') {
          this.fakeImage = fakeImage;
          var ckhootsuiteNode = editor.restoreRealElement(fakeImage);
          this.ckhootsuiteNode = ckhootsuiteNode;
          this.setupContent(ckhootsuiteNode);
        }
      }
    }
  }

  CKEDITOR.dialog.add('hootsuite', function (editor) {
    return hootsuiteDialog(editor);
  });
})();
