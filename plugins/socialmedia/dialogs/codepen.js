/**
 * @file
 */

(function () {
  // Variable used to track when the form is submitted.
  // i.e to check for double submissions on using enter key in dialog.
  var formSubmitted = false;
  var codepenDialog = function (editor) {
    return {
      title : 'CodePen Embed Properties',
      minWidth : 625,
      minHeight : 150,
      contents: [{
        id: 'codepen',
        label: 'codepen',
        elements:[{
          type: 'html',
          id: 'information',
          html: '<p>Note: all CodePen embeds will use &quot;click-to-load&quot; when displayed. Also note that CodePen does not allow embedding anonymous pens.</p>'
        },{
          type: 'text',
          id: 'idInput',
          label: 'CodePen ID: (required) ',
          setup: function (element) {
                   this.setValue(element.getAttribute('data-url'));
          }
        },{
          type: 'text',
          id: 'usernameInput',
          label: 'CodePen username: (required)',
          setup: function (element) {
                   this.setValue(element.getAttribute('data-username'));
          }
        },{
          type: 'text',
          id: 'displaynameInput',
          label: 'Author name for display: (required)',
          setup: function (element) {
                   this.setValue(element.getAttribute('data-displayname'));
          }
        },{
          type: 'text',
          id: 'titleInput',
          label: 'Title for display: (required)',
          setup: function (element) {
                   this.setValue(element.getAttribute('data-title'));
          }
        },{
          type: 'select',
          id: 'tabsInput',
          label: 'Default tab(s) to open: (required):',
          items: [
                   ['Result only', 'result'],
                   ['HTML only', 'html'],
                   ['CSS only', 'css'],
                   ['JS only', 'js'],
                   ['HTML + Result', 'html,result'],
                   ['CSS + Result', 'css,result'],
                   ['JS + Result', 'js,result']
                 ],
          setup: function (element) {
                   this.setValue(element.getAttribute('data-tabs'));
          },
        },{
            type: 'html',
            id: 'tabsInformation',
            html: '<p>At narrow page widths, CodePen will only open one tab regardless of selection. If you pick an option that is not present in the CodePen, that option will not display.</p>'
        },{
          type: 'text',
          id: 'heightInput',
          label: 'CodePen embed height in pixels (minimum 100): (required)',
          setup: function (element) {
                   this.setValue(element.getAttribute('data-height'));
          }
        }]
      }],
      onOk: function () {
        // Get form information.
        idInput = this.getValueOf('codepen','idInput');
        usernameInput = this.getValueOf('codepen','usernameInput');
        displaynameInput = this.getValueOf('codepen','displaynameInput');
        titleInput = this.getValueOf('codepen','titleInput');
        tabsInput = this.getValueOf('codepen','tabsInput');
        heightInput = this.getValueOf('codepen','heightInput');
        // Validate input. Note that there is probably a CKEditor specific way to do this, but this works.
        errors = '';
        if (!idInput) {
          errors += "You must enter the ID of the CodePen embed.\r\n";
        }
        else if (!CKEDITOR.socialmedia.codepen_id_regex.test(idInput)) {
          errors += "You must enter a valid ID for the CodePen embed.\r\n";
        }
        if (!usernameInput) {
          errors += "You must enter the username of the CodePen embed.\r\n";
        }
        else if (!CKEDITOR.socialmedia.codepen_username_regex.test(usernameInput)) {
          errors += "You must enter a valid username for the CodePen embed.\r\n";
        }
        else if (usernameInput == 'anon') {
          errors += "CodePen does not allow embedding anonymous pens.\r\n";
        }
        if (!displaynameInput) {
          errors += "You must enter an author name for the CodePen embed.\r\n";
        }
        if (!titleInput) {
          errors += "You must enter a title for the CodePen embed.\r\n";
        }
        if (!tabsInput) {
          errors += "You must select default tabs to open.\r\n";
        }
        if (!heightInput || heightInput.NaN || heightInput < 100 || Math.floor(heightInput) != heightInput) {
          errors += "You must enter a valid whole number for the height.\r\n";
        }
        // If form has been submitted before then set it back to not being seeing before.
        // i.e if this is double submission set it back to not being run before.
        if (formSubmitted == true) {
          formSubmitted = false;
          return false;
        }
        // Only display errors if there are errors to display and the form has not been run before.
        else if (errors && formSubmitted == false) {
          alert(errors);
          formSubmitted = true;
          return false;
        }
        else {
          // Create the ckcodepen element.
          var ckcodepenNode = new CKEDITOR.dom.element('ckcodepen');
          // Save contents of dialog as attributes of the element.
          ckcodepenNode.setAttribute('data-url',idInput);
          ckcodepenNode.setAttribute('data-username',usernameInput);
          ckcodepenNode.setAttribute('data-displayname',displaynameInput);
          ckcodepenNode.setAttribute('data-title',titleInput);
          ckcodepenNode.setAttribute('data-tabs',tabsInput);
          ckcodepenNode.setAttribute('data-height',heightInput);
          // Adjust title based on user input.
          CKEDITOR.lang.en.fakeobjects.ckcodepen = CKEDITOR.socialmedia.ckcodepen + ': ' + titleInput;
          // Create the fake image for this element and insert into the document (realElement, className, realElementType, isResizable).
          var newFakeImage = editor.createFakeElement(ckcodepenNode, 'ckcodepen', 'ckcodepen', false);
          // Set the fake object to the entered height; if there isn't one, use 100 so it's not invisible.
          newFakeImage.$.height = heightInput;
          newFakeImage.addClass('ckcodepen');
          if (this.fakeImage) {
            newFakeImage.replace(this.fakeImage);
            editor.getSelection().selectElement(newFakeImage);
          }
          else {
            editor.insertElement(newFakeImage);
          }
          // Reset title.
          CKEDITOR.lang.en.fakeobjects.ckcodepen = CKEDITOR.socialmedia.ckcodepen;
        }
      },
      onShow: function () {
        // Set up to handle existing items.
        this.fakeImage = this.ckcodepenNode = null;
        var fakeImage;

        // Check if element is right clicked or icon was clicked, if not use global varaible doubleclick_element.
        if (this.getSelectedElement()) {
          fakeImage = this.getSelectedElement();
        }
        else {
          fakeImage = doubleclick_element;
        }

        if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'ckcodepen') {
          this.fakeImage = fakeImage;
          var ckcodepenNode = editor.restoreRealElement(fakeImage);
          this.ckcodepenNode = ckcodepenNode;
          this.setupContent(ckcodepenNode);
        }
      }
    }
  }

  CKEDITOR.dialog.add('codepen', function (editor) {
    return codepenDialog(editor);
  });
})();
