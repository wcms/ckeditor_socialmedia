/**
 * @file
 */

(function () {
  // Variable used to track when the form is submitted.
  // i.e to check for double submissions on using enter key in dialog.
  var formSubmitted = false;
  var facebookDialog = function (editor) {
    return {
      title : 'Facebook Properties',
      minWidth : 625,
      minHeight : 100,
      contents: [{
        id: 'facebook',
        label: 'facebook',
        elements:[{
            type: 'select',
            id: 'feedType',
            label: 'Select your Facebook Feed Type:',
            items: [
                ['Timeline', 'timeline'],
                ['Single Post', 'singlepost']
            ],
            required: true,
            setup: function (element) {
                type = element.getAttribute('data-type');
                // Widgets without type are assumed to be timelines.
                if (!type) {
                  type = 'timeline';
                }
                this.setValue(type);
            },
            onChange: function () {
                adjustFeedType();
            }
        },{
            type: 'html',
            id: 'description',
            html: '',
        },{
            type: 'text',
            id: 'facebookInput',
            label: 'Facebook page name: facebook.com/',
            setup: function (element) {
                this.setValue(element.getAttribute('data-username'));
            }
        },{
            type: 'text',
            id: 'displaynameInput',
            label: 'Display name:',
            setup: function (element) {
                   this.setValue(element.getAttribute('data-displayname'));
          }
        },{
            type: 'text',
            id: 'fbUrlInput',
            label: 'URL of single post:',
            setup: function (element) {
                this.setValue(element.getAttribute('data-fburl'));
            }
        },{
            type: 'select',
            id: 'fullPost',
            label: 'Include full text of photo/video posts:',
            items: [
                ['Yes', 'true'],
                ['No', 'false']
            ],
            required: true,
            setup: function (element) {
                this.setValue(element.getAttribute('data-fullpost'));
            }
        }]
      }],
      onOk: function () {
        // Get form information.
          feedType = this.getValueOf('facebook','feedType');
          facebookInput = this.getValueOf('facebook','facebookInput');
          displaynameInput = this.getValueOf('facebook','displaynameInput');
          fbUrlInput = this.getValueOf('facebook','fbUrlInput');
          fullPost = this.getValueOf('facebook','fullPost');
        // Validate input. Note that there is probably a CKEditor specific way to do this, but this works.
        errors = '';
        switch (feedType) {
            case 'timeline':
              if (!CKEDITOR.socialmedia.facebook_username_regex_1.test(facebookInput) || !CKEDITOR.socialmedia.facebook_username_regex_2.test(facebookInput)) {
                  errors += "You must enter a valid Facebook user name.\r\n";
              }
              else if (!displaynameInput) {
                  errors += "You must enter a display name.\r\n";
              }
              break;

            case 'singlepost':
              if (! CKEDITOR.socialmedia.facebook_url_regex_1.test(fbUrlInput)) {
                  errors += "You must enter a valid embed url.\r\n";
              }
              else if (!fullPost) {
                  errors += "You must select a full text option.\r\n";
              }
              else if (!displaynameInput) {
                  errors += "You must enter a display name.\r\n";

              }
              break;
        }

        var feedtypes = ["timeline", "singlepost"];
        if (feedtypes.indexOf(feedType) == -1) {
          errors = 'Invalid type.';
        }
        // If form has been submitted before then set it back to not being seeing before.
        // i.e if this is double submission set it back to not being run before.
        if (formSubmitted == true) {
          formSubmitted = false;
          return false;
        }
        // Only display erros if there are errors to display and the form has not been run before.
        else if (errors && formSubmitted == false) {
          alert(errors);
          formSubmitted = true;
          return false;
        }
        else {
          // Create the ckfacebook element.
          var ckfacebookNode = new CKEDITOR.dom.element('ckfacebook');

          // Save contents of dialog as attributes of the element.
          ckfacebookNode.setAttribute('data-type',feedType);
          ckfacebookNode.setAttribute('data-username',facebookInput);
          ckfacebookNode.setAttribute('data-displayname',displaynameInput);
          ckfacebookNode.setAttribute('data-fburl',fbUrlInput);
          ckfacebookNode.setAttribute('data-fullpost',fullPost);
          // Adjust title based on user input.
          if (feedType == "singlepost") {
            CKEDITOR.lang.en.fakeobjects.ckfacebook = 'Facebook post: ' + displaynameInput;
          }
          else {
            // Anything that isn't a single post is assumed to be a timeline.
            // This allows widgets created before we supported anything else to work.
            CKEDITOR.lang.en.fakeobjects.ckfacebook = 'Facebook timeline: ' + displaynameInput;
          }

          // Create the fake image for this element and insert into the document (realElement, className, realElementType, isResizable).
          var newFakeImage = editor.createFakeElement(ckfacebookNode, 'ckfacebook', 'ckfacebook', false);
          if (this.fakeImage) {
            newFakeImage.replace(this.fakeImage);
            editor.getSelection().selectElement(newFakeImage);
          }
          else {
            editor.insertElement(newFakeImage);
          }
          // Reset title.
          CKEDITOR.lang.en.fakeobjects.ckfacebook = CKEDITOR.socialmedia.ckfacebook;
        }
      },
      onShow: function () {
        this.fakeImage = this.ckfacebookNode = null;
        var fakeImage;

        // Check if element is right clicked or icon was clicked, if not use global varaible doubleclick_element.
        if (this.getSelectedElement()) {
          fakeImage = this.getSelectedElement();
        }
        else {
          fakeImage = doubleclick_element;
        }

        if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'ckfacebook') {
          this.fakeImage = fakeImage;
          var ckfacebookNode = editor.restoreRealElement(fakeImage);
          this.ckfacebookNode = ckfacebookNode;
          this.setupContent(ckfacebookNode);
        }
      }
    }
  }

  CKEDITOR.dialog.add('facebook', function (editor) {
    return facebookDialog(editor);
  });

  function adjustFeedType() {
      // Show and hide form elements based on which Facebook feed type is selected.
      dialog = CKEDITOR.dialog.getCurrent();
      feedType = dialog.getValueOf('facebook','feedType');
      facebookInput = dialog.getContentElement('facebook','facebookInput').getElement();
      displaynameInput = dialog.getContentElement('facebook','displaynameInput').getElement();
      fbUrlInput = dialog.getContentElement('facebook','fbUrlInput').getElement();
      fullPost = dialog.getContentElement('facebook','fullPost').getElement();
      description = dialog.getContentElement('facebook','description').getElement();
      if (feedType == 'timeline') {
          facebookInput.show();
          displaynameInput.show();
          fullPost.hide();
          fbUrlInput.hide();
          description.setHtml('Show the user&rsquo;s posts in timeline form.');
      }
      else if (feedType == 'singlepost') {
          facebookInput.hide();
          displaynameInput.show();
          fullPost.show();
          fbUrlInput.show();
          description.setHtml('Show a single post.');
      }
    }
})();
