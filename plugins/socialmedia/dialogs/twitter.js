/**
 * @file
 */

(function () {
  // Variable used to track when the form is submitted.
  // i.e to check for double submissions on using enter key in dialog.
  var formSubmitted = false;
  var twitterDialog = function (editor) {
    return {
      title : 'Twitter Properties',
      minWidth : 625,
      minHeight : 200,
      contents: [{
        id: 'Twitter',
        label: 'Twitter User',
        elements:[{
          type: 'select',
          id: 'twittertypeInput',
          label: 'Twitter widget type:',
          items: [
                   ['Profile', 'profile'],
                   ['Likes', 'faves'],
                   ['List', 'list'],
                   ['Embedded Tweet', 'tweet'],
                   ['Collection timeline', 'custom'],
                   ['Collection grid', 'grid'],
                   ['Moment', 'moment']
                 ],
          required: true,
          setup: function (element) {
                   this.setValue(element.getAttribute('data-type'));
          },
          onChange: function () {
                      adjustfortype();
          }
        },{
          type: 'html',
          id: 'description',
          html: '',
        },{
          type: 'text',
          id: 'twitterInput',
          label: 'Twitter user name: @',
          setup: function (element) {
                   this.setValue(element.getAttribute('data-username'));
          }
        },{
          type: 'text',
          id: 'listInput',
          label: 'List name:',
          setup: function (element) {
                   this.setValue(element.getAttribute('data-listname'));
          }
        },{
          type: 'textarea',
          rows: 4,
          cols: 40,
          id: 'tweetInput',
          label: 'Embedded Tweet code from Twitter or individual Tweet URL:',
          setup: function (element) {
                   this.setValue(element.getAttribute('data-tweet'));
          }
        },{
          type: 'text',
          id: 'urlInput',
          label: 'Twitter URL fragment: https://twitter.com/',
          setup: function (element) {
                   this.setValue(element.getAttribute('data-url'));
          }
        },{
          type: 'text',
          id: 'timelineInput',
          label: 'Timeline name:',
          setup: function (element) {
                   this.setValue(element.getAttribute('data-timeline'));
          }
        }]
      }],
      onShow: function () {
        // Set up to handle existing items.
        this.fakeImage = this.cktwitterNode = null;
        var fakeImage;

        // Check if element is right clicked or icon was clicked, if not use global varaible doubleclick_element.
        if (this.getSelectedElement()) {
          fakeImage = this.getSelectedElement();
        }
        else {
          fakeImage = doubleclick_element;
        }

        if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'cktwitter') {
          this.fakeImage = fakeImage;
          var cktwitterNode = editor.restoreRealElement(fakeImage);
          this.cktwitterNode = cktwitterNode;
          this.setupContent(cktwitterNode);
        }
        // Set up the dialog box filtering based on type.
        adjustfortype();
      },
      onOk: function () {
        // Get form information.
        twittertype = this.getValueOf('Twitter','twittertypeInput');
        twitterInput = this.getValueOf('Twitter','twitterInput');
        listInput = this.getValueOf('Twitter','listInput');
        tweetInput = this.getValueOf('Twitter','tweetInput');
        urlInput = this.getValueOf('Twitter','urlInput');
        timelineInput = this.getValueOf('Twitter','timelineInput');
        // let's be nice and clean up some (likely) common mistakes.
        if (twitterInput.substr(0,1) == '@') {
          // Strip leading "@" from Twitter usernames.
          twitterInput = twitterInput.substr(1);
        }
        // Replace spaces with "-" in Twitter list names.
        listInput = listInput.replace(/ /gi,'-');
        // Validate input. Note that there is probably a CKEditor specific way to do this, but this works.
        var errors = '';
        switch (twittertype) {
          case 'profile':
          case 'faves':
            if (!CKEDITOR.socialmedia.twitter_username_regex.test(twitterInput)) {
              errors += "You must enter a valid Twitter user name.\r\n";
            }
            break;

          case 'list':
            if (!CKEDITOR.socialmedia.twitter_username_regex.test(twitterInput)) {
              if (!CKEDITOR.socialmedia.twitter_listname_regex.test(listInput)) {
                errors += "You must enter a valid Twitter user name and list name.\r\n";
              }
              else {
                errors += "You must enter a valid Twitter user name.\r\n";
              }
            }
            else {
              if (!CKEDITOR.socialmedia.twitter_listname_regex.test(listInput)) {
                errors += "You must enter a valid Twitter list name.\r\n";
              }
            }
            break;

          case 'tweet':
            if (!CKEDITOR.socialmedia.twitter_tweet_regex.test(tweetInput)) {
              errors += "You must enter a valid embedded Tweet code from Twitter or individual Tweet URL.\r\n";
            }
            break;

          case 'custom':
          case 'grid':
            if (!CKEDITOR.socialmedia.twitter_url_regex.test(urlInput)) {
              errors += "You must enter a valid embedded collection URL fragment.\r\n";
            }
            if (!CKEDITOR.socialmedia.twitter_timelineinput_regex.test(timelineInput)) {
              errors += "You must enter a valid embedded collection name.\r\n";
            }
            break;

          case 'moment':
            if (!CKEDITOR.socialmedia.twitter_moment_url_regex.test(urlInput)) {
              errors += "You must enter a valid moment URL fragment.\r\n";
            }
            break;
        }

        var twittertypes = ["profile", "faves", "list", "tweet", "custom", "grid", "moment"];
        if (twittertypes.indexOf(twittertype) == -1) {
          errors = 'Invalid widget type.';
        }
        // If form has been submitted before then set it back to not being seeing before.
        // i.e if this is double submission set it back to not being run before.
        if (formSubmitted == true) {
          formSubmitted = false;
          return false;
        }
        // Only display erros if there are errors to display and the form has not been run before.
        else if (errors && formSubmitted == false) {
          alert(errors);
          formSubmitted = true;
          return false;
        }
        else {
          // Create the cktwitter element.
          var cktwitterNode = new CKEDITOR.dom.element('cktwitter');
          // Save contents of dialog as attributes of the element.
          cktwitterNode.setAttribute('data-type',twittertype);
          cktwitterNode.setAttribute('data-username',twitterInput);
          cktwitterNode.setAttribute('data-listname',listInput);
          cktwitterNode.setAttribute('data-tweet',tweetInput);
          cktwitterNode.setAttribute('data-url',urlInput);
          cktwitterNode.setAttribute('data-timeline',timelineInput);
          // Adjust title based on user input.
          widgettext = 'Twitter widget: ';
          switch (twittertype) {
            case 'profile':
              widgettext += 'Tweets by @' + twitterInput;
              break;

            case 'faves':
              widgettext += 'Likes from @' + twitterInput;
              break;

            case 'list':
              widgettext += 'Tweets from @' + twitterInput + '/' + listInput;
              break;

            case 'tweet':
              widgettext += 'Embedded Tweet';
              break;

            case 'custom':
              widgettext += 'Collection timeline "' + timelineInput + '"';
              break;

            case 'grid':
              widgettext += 'Collection grid "' + timelineInput + '"';
              break;

            case 'moment':
              widgettext += 'Moment';
              break;
          }
          CKEDITOR.lang.en.fakeobjects.cktwitter = widgettext;
          // Create the fake image for this element and insert into the document (realElement, className, realElementType, isResizable).
          var newFakeImage = editor.createFakeElement(cktwitterNode, 'cktwitter', 'cktwitter', false);
          newFakeImage.addClass('cktwitter');
          if (this.fakeImage) {
            newFakeImage.replace(this.fakeImage);
            editor.getSelection().selectElement(newFakeImage);
          }
          else {
            editor.insertElement(newFakeImage);
          }
          // Reset title.
          CKEDITOR.lang.en.fakeobjects.cktwitter = CKEDITOR.socialmedia.cktwitter;
        }
      },
    }
  }

  CKEDITOR.dialog.add('twitter', function (editor) {
    return twitterDialog(editor);
  });

  function adjustfortype() {
    // Show and hide form elements based on which Twitter type is selected.
    dialog = CKEDITOR.dialog.getCurrent();
    twittertype = dialog.getValueOf('Twitter','twittertypeInput');
    twitterInput = dialog.getContentElement('Twitter','twitterInput').getElement();
    listInput = dialog.getContentElement('Twitter','listInput').getElement();
    tweetInput = dialog.getContentElement('Twitter','tweetInput').getElement();
    urlInput = dialog.getContentElement('Twitter','urlInput').getElement();
    timelineInput = dialog.getContentElement('Twitter','timelineInput').getElement();
    description = dialog.getContentElement('Twitter','description').getElement();
    if (twittertype == 'profile') {
      twitterInput.show();
      listInput.hide();
      tweetInput.hide();
      urlInput.hide();
      timelineInput.hide();
      description.setHtml('Show the user&rsquo;s most recent tweets.');
    }
    else if (twittertype == 'faves') {
      twitterInput.show();
      listInput.hide();
      tweetInput.hide();
      urlInput.hide();
      timelineInput.hide();
      description.setHtml('Show the user&rsquo;s most recent liked tweets.');
    }
    else if (twittertype == 'list') {
      twitterInput.show();
      listInput.show();
      tweetInput.hide();
      urlInput.hide();
      timelineInput.hide();
      description.setHtml('Show the most recent tweets from a user&rsquo;s public list.');
    }
    else if (twittertype == 'tweet') {
      twitterInput.hide();
      listInput.hide();
      tweetInput.show();
      urlInput.hide();
      timelineInput.hide();
      description.setHtml('Show a specific tweet.');
    }
    else if (twittertype == 'custom') {
      twitterInput.hide();
      listInput.hide();
      tweetInput.hide();
      urlInput.show();
      timelineInput.show();
      description.setHtml('Show an embedded collection timeline of tweets.');
    }
    else if (twittertype == 'grid') {
      twitterInput.hide();
      listInput.hide();
      tweetInput.hide();
      urlInput.show();
      timelineInput.show();
      description.setHtml('Show an embedded collection grid of tweets.');
    }
    else if (twittertype == 'moment') {
      twitterInput.hide();
      listInput.hide();
      tweetInput.hide();
      urlInput.show();
      timelineInput.hide();
      description.setHtml('Show a moment tweet.');
    }
  }
})();
