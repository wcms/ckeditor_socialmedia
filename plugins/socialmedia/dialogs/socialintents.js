/**
 * @file
 */

(function () {
  // Variable used to track when the form is submitted.
  // i.e to check for double submissions on using enter key in dialog.
  var formSubmitted = false;
  var socialintentsDialog = function (editor) {
    return {
      title : 'Social Intents Embed Properties',
      minWidth : 625,
      minHeight : 150,
      contents: [{
        id: 'socialintents',
        label: 'socialintents',
        elements:[{
          type: 'text',
          id: 'usernameInput',
          label: 'Social Intents username: (required)',
          setup: function (element) {
                   this.setValue(element.getAttribute('data-username'));
          }
        },{
            type: 'html',
            id: 'information',
            html: '<p>This information is at the end of your standalone URL (e.g. https://chat.socialintents.com/c/<strong>livechatsi</strong>)</p>'
        }]
      }],
      onOk: function () {
        // Get form information.
        usernameInput = this.getValueOf('socialintents','usernameInput');
        // Validate input. Note that there is probably a CKEditor specific way to do this, but this works.
        errors = '';
        if (!usernameInput) {
          errors += "You must enter the username for the Social Intents embed.\r\n";
        }
        else if (!CKEDITOR.socialmedia.socialintents_username_regex.test(usernameInput)) {
          errors += "You must enter a valid username for the Social Intents embed.\r\n";
        }
        // If form has been submitted before then set it back to not being seeing before.
        // i.e if this is double submission set it back to not being run before.
        if (formSubmitted == true) {
          formSubmitted = false;
          return false;
        }
        // Only display errors if there are errors to display and the form has not been run before.
        else if (errors && formSubmitted == false) {
          alert(errors);
          formSubmitted = true;
          return false;
        }
        else {
          // Create the cksocialintents element.
          var cksocialintentsNode = new CKEDITOR.dom.element('cksocialintents');
          // Save contents of dialog as attributes of the element.
          cksocialintentsNode.setAttribute('data-username',usernameInput);
          // Adjust title based on user input.
          CKEDITOR.lang.en.fakeobjects.cksocialintents = CKEDITOR.socialmedia.cksocialintents + ': ' + usernameInput;
          // Create the fake image for this element and insert into the document (realElement, className, realElementType, isResizable).
          var newFakeImage = editor.createFakeElement(cksocialintentsNode, 'cksocialintents', 'cksocialintents', false);
          // Set the fake object to height.
          newFakeImage.$.height = 100;
          newFakeImage.addClass('cksocialintents');
          if (this.fakeImage) {
            newFakeImage.replace(this.fakeImage);
            editor.getSelection().selectElement(newFakeImage);
          }
          else {
            editor.insertElement(newFakeImage);
          }
          // Reset title.
          CKEDITOR.lang.en.fakeobjects.cksocialintents = CKEDITOR.socialmedia.cksocialintents;
        }
      },
      onShow: function () {
        // Set up to handle existing items.
        this.fakeImage = this.cksocialintentsNode = null;
        var fakeImage;

        // Check if element is right clicked or icon was clicked, if not use global variable doubleclick_element.
        if (this.getSelectedElement()) {
          fakeImage = this.getSelectedElement();
        }
        else {
          fakeImage = doubleclick_element;
        }

        if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'cksocialintents') {
          this.fakeImage = fakeImage;
          var cksocialintentsNode = editor.restoreRealElement(fakeImage);
          this.cksocialintentsNode = cksocialintentsNode;
          this.setupContent(cksocialintentsNode);
        }
      }
    }
  }

  CKEDITOR.dialog.add('socialintents', function (editor) {
    return socialintentsDialog(editor);
  });
})();
